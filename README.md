# Flight planner

Click on the pen icon to draw a flight
Click on airplane icon to list and view saved flights

## Install

```
$ yarn
```

## Run

```
$ yarn run
```

## Build

```
$ yarn build
```