import store from './index'

// types
import { AppAction } from './app.types'

export const OPEN_FLIGHT_LIST = 'APP:OPEN_FLIGHT_LIST'
export const openFlightList = (): AppAction =>
  store.dispatch({ type: OPEN_FLIGHT_LIST })

export const CLOSE_FLIGHT_LIST = 'APP:CLOSE_FLIGHT_LIST'
export const closeFlightList = (): AppAction =>
  store.dispatch({ type: CLOSE_FLIGHT_LIST })

export const START_DRAWING = 'APP:START_DRAWING'
export const startDrawing = (): AppAction =>
  store.dispatch({ type: START_DRAWING })

export const STOP_DRAWING = 'APP.STOP_DRAWING'
export const stopDrawing = (): AppAction =>
  store.dispatch({ type: STOP_DRAWING })

export const SHOW_FLIGHT = 'APP.SHOW_FLIGHT'
export const showFlight = (flightName: string): AppAction | null => {
  const flight = store.getState().flightList.find(({ name }) => name === flightName)
  return flight
    ? store.dispatch({ type: SHOW_FLIGHT, payload: flight.line })
    : null
}