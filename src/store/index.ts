import { createStore, combineReducers } from 'redux'
import map from './map.reducer'
import flightList from './flightList.reducer'
import app from './app.reducer'

export default createStore(combineReducers({
  app,
  flightList,
  map,
}))