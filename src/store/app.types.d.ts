export interface AppState {
  isDrawing: boolean
  flightListIsOpen: boolean
  saveFlightModalIsOpen: boolean
}

export interface AppAction {
  type: string
}