import { has, dropLast, prop } from 'ramda'
import {
  ADD_POINT,
  SET_LAST_POINT,
} from './map.actions'
import {
  STOP_DRAWING, SHOW_FLIGHT, START_DRAWING,
} from './app.actions'
import epflFlight from './epfl.flight'

// types
import { MapState, MapAction, PointAction, LineAction, OtherAction } from './map.types'
import { SAVE_FLIGHT } from './flightList.actions'

const defaultState: MapState = {
  position: [46.5192374200743,6.57],
  zoom: 17,
  line: epflFlight.line,
  /*
    Specs say "When the application is loaded, it shows a map..."
    Should be line: [] otherwise
  */
}

const currentLine = prop('line')

const isPointAction = (action: MapAction): action is PointAction =>
  has('payload', action)
  && !isNaN(Number(action.payload[0]))
  && !isNaN(Number(action.payload[1]))

const isLineAction = (action: MapAction): action is LineAction =>
  has('payload', action)
  && Array.isArray(action.payload)
  && Array.isArray(action.payload[0])

const isOtherAction = (action: MapAction): action is OtherAction =>
  !isPointAction(action) && !isLineAction(action)

const handlePoint = (state: MapState, { type, payload }: PointAction): MapState => {
  switch (type) {
    case SET_LAST_POINT:
      return { ...state, line: [...dropLast(1, currentLine(state)), payload] }
    case ADD_POINT:
      return { ...state, line: [...currentLine(state), payload] }
    default:
      return state
  }
}

const handleLine = (state: MapState, { type, payload }: LineAction): MapState => {
  switch (type) {
    case SHOW_FLIGHT:
      return { ...state, line: payload }
    default:
      return state
  }
}

const handleOther = (state: MapState, { type }: OtherAction): MapState => {
  switch (type) {
    case STOP_DRAWING:
      return { ...state, line: dropLast(1, currentLine(state)) }
    case SAVE_FLIGHT:
      return { ...state, line: [] }
    case START_DRAWING:
      return { ...state, line: [] }
    default:
      return state
  }
}

export default (state: MapState = defaultState, action: MapAction): MapState => {
  if (isPointAction(action)) {
    return handlePoint(state, action)
  }
  if (isLineAction(action)) {
    return handleLine(state, action)
  }
  if (isOtherAction(action)) {
    return handleOther(state, action)
  }
  return state
}