import store from './index'

// types
import { Point, MapAction } from './map.types'

export const SET_LAST_POINT = 'MAP:SET_LAST_POINT'
export const setLastPoint = (point: Point): MapAction|null =>
  store.getState().app.isDrawing
    ? store.dispatch({
        type: SET_LAST_POINT,
        payload: point,
      })
    : null

export const ADD_POINT = 'MAP:ADD_POINT'
export const addPoint = (point: Point): MapAction|null =>
  store.getState().app.isDrawing
  ? store.dispatch({
      type: ADD_POINT,
      payload: point,
    })
  : null