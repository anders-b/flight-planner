import { SAVE_FLIGHT } from './flightList.actions'
import epflFlight from './epfl.flight'

// types
import { FlightListAction, FlightListState } from './flightList.types'

const defaultState: FlightListState = [
  epflFlight,
  /*
    Specs say "When the application is loaded, it shows a map..."
    Should be [] otherwise
  */
]

export default (state: FlightListState = defaultState, {type, payload}: FlightListAction): FlightListState => {
  switch (type) {
    case SAVE_FLIGHT:
      return [...state, payload]
    default:
      return state
  }
}