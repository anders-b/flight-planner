import { MapState } from './map.types'
import { FlightListState } from './flightList.types'
import { AppState } from './app.types'

export interface State {
  map: MapState
  flightList: FlightListState
  app: AppState
}