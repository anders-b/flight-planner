export type Point = [number, number]

export interface MapState {
  position: Point
  zoom: number
  line: Point[]
}

export interface PointAction {
  type: string
  payload: Point
}

export interface LineAction {
  type: string
  payload: Point[]
}

export interface OtherAction {
  type: string
  payload?: any
}

export type MapAction = PointAction | LineAction | OtherAction
