import {
  OPEN_FLIGHT_LIST,
  CLOSE_FLIGHT_LIST,
  START_DRAWING,
  STOP_DRAWING,
  SHOW_FLIGHT,
} from './app.actions'
import {
  SAVE_FLIGHT,
} from './flightList.actions'

// types 
import { AppAction, AppState } from './app.types'

const defaultState: AppState = {
  isDrawing: false,
  flightListIsOpen: false,
  saveFlightModalIsOpen: false,
}

export default (state: AppState = defaultState, { type }: AppAction): AppState => {
  switch (type) {
    case OPEN_FLIGHT_LIST:
      return { ...state, flightListIsOpen: true }
    case CLOSE_FLIGHT_LIST:
      return { ...state, flightListIsOpen: false }
    case START_DRAWING:
      return { ...state, isDrawing: true }
    case STOP_DRAWING:
      return { ...state, isDrawing: false, saveFlightModalIsOpen: true }
    case SAVE_FLIGHT:
      return { ...state, saveFlightModalIsOpen: false  }
    case SHOW_FLIGHT:
      return { ...state, flightListIsOpen: false }
    default: 
      return state
  }
}
