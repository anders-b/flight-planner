import store from './index'

// types
import { FlightListAction } from './flightList.types'

export const SAVE_FLIGHT = 'FLIGHT_LIST:SAVE'
export const saveFlight = (name: string): FlightListAction =>
  store.dispatch({
    type: SAVE_FLIGHT,
    payload: {
      name,
      line: store.getState().map.line,
    },
  })
