import { Point } from './map.types'

export interface Flight {
  name: string
  line: Point[]
}

export type FlightListState = Flight[]

export interface FlightListAction {
  type: string
  payload: Flight
}