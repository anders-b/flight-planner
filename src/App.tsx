import * as React from 'react'
import Map from './components/Map'
import Menu from './components/Menu'
import FlightList from './components/FlightList'
import SaveFlight from './components/SaveFlight'
import { connect } from 'react-redux'

// types
import { State } from './store/types'
import { AppState } from './store/app.types'
import { FlightListState } from './store/flightList.types'

// css
import './App.css'

interface Props extends AppState {
  savedFlights: FlightListState
}

const App = ({
  isDrawing,
  flightListIsOpen,
  savedFlights,
  saveFlightModalIsOpen,
}: Props) =>
  <div id="container">
    <Map />
    <Menu
      isDrawing={ isDrawing }/>
    <FlightList
      flightListIsOpen={ flightListIsOpen }
      savedFlights={ savedFlights }
      />
    <SaveFlight saveFlightModalIsOpen={ saveFlightModalIsOpen } />
  </div>

const mapStateToProps = ({ app, flightList }: State): Props => ({
  ...app,
  savedFlights: flightList,
})

export default connect(mapStateToProps)(App)