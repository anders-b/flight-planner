import * as React from 'react'
import { isEmpty } from 'ramda'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListSubheader from '@material-ui/core/ListSubheader'
import { closeFlightList, showFlight } from '../store/app.actions'

// types
import { Flight } from '../store/flightList.types'

const onClose = () => closeFlightList()

interface FlightListProps {
  flightListIsOpen: boolean
  savedFlights: Flight[]
}

const FlightListItem = ({ name }: Flight, index: number) =>
  <ListItem
    key={ index }
    onClick={ () => showFlight(name) }
    >{ name }</ListItem>

const Flights = ({ savedFlights }: { savedFlights: Flight[] }) =>
  isEmpty(savedFlights)
    ? <List>
        <ListItem>You have no saved flights</ListItem>
      </List>
    : <List>
        { savedFlights.map(FlightListItem) }
      </List>

export default ({ flightListIsOpen, savedFlights }: FlightListProps) =>
  <Drawer
    anchor="right"
    open={ flightListIsOpen }
    onClose={ onClose }
    >
    <ListSubheader>Your saved flights</ListSubheader>
    <Flights savedFlights={ savedFlights } />
  </Drawer>