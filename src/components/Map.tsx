import * as React from 'react'
import { Map as LeafletMap, TileLayer } from 'react-leaflet'
import { connect } from 'react-redux'
import { addPoint, setLastPoint } from '../store/map.actions'
import FlightPath from './FlightPath'

// css
import 'leaflet/dist/leaflet.css'

// types
import { State } from '../store/types'
import { MapState, Point } from '../store/map.types'

const eventToPoint = ({ latlng }: any): Point =>
  ([latlng.lat, latlng.lng])

const onMouseMove = (e: any) =>
  setLastPoint(eventToPoint(e))

const onClick = (e: any) =>
  addPoint(eventToPoint(e))

const Map = ({ position, zoom, line }: MapState) =>
  <LeafletMap
    center={position}
    zoom={zoom}
    onMouseMove={ onMouseMove }
    onClick={ onClick }
    >
    <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'/>
    <FlightPath positions={line}/>
  </LeafletMap>

export default connect(({ map }: State) => map)(Map)