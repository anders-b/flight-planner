import * as React from 'react'
import { CircleMarker, Polyline } from 'react-leaflet'

// types
import { Point } from '../store/map.types'


interface FlightPathProps {
  positions: Point[]
}

export default ({ positions }: FlightPathProps) => {
  switch (positions.length) {
    case 0:
      return null
    case 1:
      return <CircleMarker center={ positions[0] } radius={ 2 } />
    default:
      return <Polyline positions={ positions } />
  }
}