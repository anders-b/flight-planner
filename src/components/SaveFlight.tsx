import * as React from 'react'
import Drawer from '@material-ui/core/Drawer'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import { saveFlight } from '../store/flightList.actions'

interface Props {
  saveFlightModalIsOpen: boolean
}

type State = {
  name: string
}

class SaveFlight extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = { name: '' }
  }

  onChange = (e: any) =>
    this.setState({ name: e.target.value })
  
  onSubmit = () => {
    saveFlight(this.state.name)
    this.setState({ name: '' })
  }

  public render() {
    return <Drawer
    anchor="right"
    open={ this.props.saveFlightModalIsOpen }
    >
      <form>
        <TextField
          label="Name of the flight"
          value={ this.state.name }
          onChange={ this.onChange }
          margin="normal"
          variant="outlined"
          style={{ margin: 'auto', padding: '5px', marginTop: '50px' }}
          />
        <br/>
        <Button
          color="primary"
          variant="contained"
          style={{ marginLeft: '5px', marginTop: '10px' }}
          onClick={ this.onSubmit }
          >Save</Button>
      </form>
    </Drawer>
  }
}

export default SaveFlight