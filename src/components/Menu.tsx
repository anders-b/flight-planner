import * as React from 'react'
import Button from '@material-ui/core/Button'
import DrawIcon from '@material-ui/icons/Edit'
import FlightIcon from '@material-ui/icons/AirplanemodeActive'
import SaveIcon from '@material-ui/icons/Save'
import { openFlightList, startDrawing, stopDrawing } from '../store/app.actions'

const buttonStyle = {
  marginRight: '10px',
}

interface MenuProps {
  isDrawing: boolean
}

const DrawOrSave = ({ isDrawing }: MenuProps) =>
  isDrawing
    ? <Button
        variant="fab"
        color="primary"
        style={buttonStyle}
        onClick={ () => stopDrawing() }>
        <SaveIcon />
      </Button>
    : <Button
        variant="fab"
        color="primary"
        style={buttonStyle}
        onClick={ () => startDrawing() }>
        <DrawIcon />
      </Button>


export default ({ isDrawing }: MenuProps) =>
  <div id="menu">
    <DrawOrSave isDrawing={ isDrawing } />
    <Button
      variant="fab"
      color="primary"
      style={buttonStyle}
      onClick={ () => openFlightList() }>
      <FlightIcon />
    </Button>
  </div>